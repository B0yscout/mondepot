#include <stdio.h>
#include <stdlib.h>

int main()
{

    int nb = 0;
    int cpt = 0;
    float som = 0;
    float moyenne = 0;
    float note = 0;

    printf("\nEntrer le nombre d'élèves:");
    scanf("%d", &nb);

    float notes[nb];



    for(cpt=0;cpt<nb;cpt++)
    {
        do
        {
            printf("\nNote de l'élève n° %d?", cpt);
            scanf("%f", &note);
            if (note < 0 || note > 20)
            {
                printf("note doit être comprise entre 0 et 20");
            }
        } while (note < 0 || note > 20);

        notes[cpt]=note;
        som+=note;
    }


    float noteMin , noteMax;
    noteMax = 0;
    noteMin = 20;

    printf("\n\nRésultats de la classe:");
    for(cpt=0;cpt<nb;cpt++)
    {
        if (noteMax < notes[cpt])
        {
            noteMax = notes[cpt];
        }

        if (noteMin > notes[cpt])
        {
            noteMin = notes[cpt];
        }
        printf("\nNote de l'élève n°%d = %f", cpt, notes[cpt]);
        if (notes[cpt]>=10)
        {
            printf(" -> ADMIS");
        }
        else
        {
            printf(" -> NON ADMIS");
        }
    }

    moyenne = som / nb;
    printf("\nla moyenne de la classe est = %.2f", moyenne);
    printf("\nnote Min : %f", noteMin);
    printf("\nnote Max : %f", noteMax);


    return 0;
}
